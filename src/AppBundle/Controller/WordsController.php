<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Words;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;



class WordsController extends Controller
{
    /**
     * @Route("/{_locale}/", requirements = {"_locale" : "en|ru|"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('ruWord', TextType::class, ['label' => 'Введите слово для перевода'])
            ->add('save', SubmitType::class, ['label' => 'Сохранить'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $words = new Words();
            $words->translate('ru')->setWord($data['ruWord']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($words);

            $words->mergeNewTranslations();
            $em->flush();
            return $this->redirectToRoute('app_words_index');
        }
        $allWords = $this->getDoctrine()
            ->getRepository('AppBundle:Words')
            ->findAll();
        return $this->render('@App\Words\index.html.twig', [
            'form' => $form->createView(),
            'words' => $allWords
        ]);
    }

    /**
     * @Route("/translate/{id}",requirements={"id": "\d+"})
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function translateAction($id, Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('enWord', TextType::class, [
                'label' => 'перевод на английсский',
                'required' => false
            ])
            ->add('frWord', TextType::class, [
                'label' => 'перевод на французский',
                'required' => false
            ])
            ->add('spWord', TextType::class, [
                'label' => 'перевод на испанский',
                'required' => false
            ])
            ->add('grWord', TextType::class, [
                'label' => 'перевод на греческий',
                'required' => false
            ])
            ->add('itWord', TextType::class, [
                'label' => 'перевод на итальянский',
                'required' => false
            ])
            ->add('save', SubmitType::class, ['label' => 'Сохранить'])
            ->getForm();

        $form->handleRequest($request);
        dump($form->getData());
        $words = $this->getDoctrine()
            ->getRepository('AppBundle:Words')
            ->find($id);

        if ($form->isSubmitted()) {
            $data = $form->getData();

            if(trim($data['enWord'])){
                $words->translate('en', false)->setWord($data['enWord']);
            }
            if (trim($data['frWord'])){
                $words->translate('fr',false)->setWord($data['frWord']);
            }
            if (trim($data['spWord'])){
                $words->translate('sp',false)->setWord($data['spWord']);
            }
            if (trim($data['grWord'])){
                $words->translate('gr', false)->setWord($data['grWord']);
            }
            if(trim($data['itWord'])){
                $words->translate('it', false)->setWord($data['itWord']);
            }

            $em = $this->getDoctrine()->getManager();

            $words->mergeNewTranslations();
            $em->flush();
            return $this->redirectToRoute('app_words_translate', ['id' => $id]);
        }

        return $this->render('@App\Words\translate.html.twig', array(
            'form' => $form->createView(),
            'words' => $words
        ));
    }

}
